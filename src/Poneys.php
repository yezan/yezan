<?php 
  class Poneys {
      private $count = 8;

      public function getCount() {
        return $this->count;
      }

      public function removePoneyFromField($number)  {
      	if ( $number < 0 )
      	{
      		throw new Exception("Negative Poney number exception", 1);
      	}
      	else
      	{
      		$this->count -= $number;
      	}
      }

      public function addPoneyToField($number) {
      	 if ( $this->count >= 15 )
      	 {
      	 	 throw new Exception("Exception : Max number of places reached", 1);
      	 }
      	 else
      	 {
      	 	$this->count += $number;	
      	 }
      }

      public function getNames()
      {
            
      }

      public function isPlaceAvaiblable()
      {
      	 return $this->count < 15;
      }

      

      public function setCount($ct)
      {
      		$this->count = $ct;
      }
  }
?>
