<?php
  require_once 'src/Poneys.php';


  class PoneysTest extends \PHPUnit_Framework_TestCase {

    private $poneys;

    public function setUp()
    {
          $this->poneys = new Poneys();
          $this->poneys->setCount(COUNT);
    }

    public function tearDown()
    {
        unset($this->poneys);
    }
    /**
     *  
     * @dataProvider      poneyProvider
     */
    public function test_removePoneyFromField($a, $expected) {
      // Setup
      // Action
      $this->poneys->removePoneyFromField($a);
  
      // Assert
      $this->assertEquals($expected, $this->poneys->getCount());
    }



    public function test_addPoneyToField() {
      // Setup
      
      // Action
      $this->poneys->addPoneyToField(2);
      
      // Assert
      $this->assertEquals(10, $this->poneys->getCount());
    }

    public function poneyProvider()
    {
       return array( 
                    array(3, 5),
                    array(2, 6), 
                    );
    }

    // public function test_getNames()
    // {
    //     $this->poneys =  $this->getMockBuilder('Poneys')->getMock();
    //     $list =    array('name1', 'name2', 'name3');
    //     $this->poneys->method('getNames')->willReturn($list);

    //     $this->assertEquals($list, $Poneys->getNames);
    // }


   
  }
 ?>
